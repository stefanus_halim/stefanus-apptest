import React from 'react';
import AppRoute from './src/routes';
import {Provider} from 'react-redux';
import {store} from './src/stores';

export default function App() {
  return (
    <Provider store={store}>
      <AppRoute initialRoute={'Contact List'} />
    </Provider>
  );
}
