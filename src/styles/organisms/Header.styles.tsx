import {StyleSheet} from 'react-native';
import {Spacing, Scale, fontFamilies, fontSizes, Colors} from '../abstract';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    elevation: 5,
    backgroundColor: Colors.white,
  },
  leftContainer: {
    flexDirection: 'row',
    width: '90%',
  },
  rightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '10%',
  },
  backIconContainer: {
    justifyContent: 'center',
    marginLeft: Scale.Horizontal(Spacing.md),
  },
  backIcon: {
    width: Scale.Horizontal(Spacing.xl),
    height: Scale.Horizontal(Spacing.xl),
  },
  title: {
    fontFamily: fontFamilies.Black,
    fontSize: Scale.Horizontal(fontSizes.xl),
    paddingVertical: Scale.Horizontal(Spacing.md),
    marginLeft: Scale.Horizontal(Spacing.md),
    color: Colors['indian-red'],
  },
});
