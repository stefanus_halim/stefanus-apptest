import {StyleSheet} from 'react-native';
import {Spacing, Scale} from '../abstract';

export default StyleSheet.create({
  outerContainer: {
    margin: Scale.Horizontal(Spacing.md),
  },
  contentContainer: {
    padding: Scale.Horizontal(Spacing.xxxs),
  },
});
