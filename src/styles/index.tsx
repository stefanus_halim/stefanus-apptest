import {StyleSheet} from 'react-native';
import {Colors} from './abstract';

export default StyleSheet.create({
  pageOuter: {
    flex: 1,
    backgroundColor: Colors.white,
  },
});
