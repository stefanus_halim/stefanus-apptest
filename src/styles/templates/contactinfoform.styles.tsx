import {StyleSheet} from 'react-native';
import {Spacing, Scale} from '../abstract';

export default StyleSheet.create({
  container: {
    marginVertical: Scale.Horizontal(Spacing.xs),
  },
  contentContainer: {
    paddingHorizontal: Scale.Horizontal(Spacing.xs),
    paddingBottom: Scale.Horizontal(Spacing.xs),
  },
});
