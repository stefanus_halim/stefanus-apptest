import {StyleSheet} from 'react-native';
import {Spacing, Scale, fontSizes, fontFamilies, Colors} from '../abstract';

export default StyleSheet.create({
  outerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  leftContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rightContainer: {
    justifyContent: 'center',
    marginLeft: Scale.Horizontal(Spacing.xs),
  },
  mainTxt: {
    fontFamily: fontFamilies.Black,
    fontSize: Scale.Horizontal(fontSizes.md),
    color: Colors['indian-red'],
  },
  secondaryTxt: {
    fontFamily: fontFamilies.Regular,
    fontSize: Scale.Horizontal(fontSizes.s),
    color: Colors.black,
  },
  photo: {
    width: 50,
    height: 50,
    borderRadius: Scale.Horizontal(50),
  },
});
