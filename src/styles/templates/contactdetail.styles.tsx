import {StyleSheet} from 'react-native';
import {Spacing, Scale, fontSizes, fontFamilies, Colors} from '../abstract';

export default StyleSheet.create({
  outerContainer: {
    margin: Scale.Horizontal(Spacing.md),
  },
  cardContainer: {
    flexDirection: 'row',
  },
  cardContentContainer: {
    marginLeft: Scale.Horizontal(Spacing.xs),
    justifyContent: 'center',
  },
  photo: {
    width: Scale.Horizontal(150),
    height: Scale.Horizontal(150),
    borderRadius: Scale.Horizontal(100),
  },
  mainTxt: {
    fontFamily: fontFamilies.Bold,
    fontSize: Scale.Horizontal(fontSizes.xl),
    color: Colors['indian-red'],
    width: Scale.Horizontal(150),
  },
  secondaryTxt: {
    fontFamily: fontFamilies.Regular,
    fontSize: Scale.Horizontal(fontSizes.md),
    color: Colors.black,
  },
});
