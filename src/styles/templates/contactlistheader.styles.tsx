import {StyleSheet} from 'react-native';
import {Spacing, Scale} from '../abstract';

export default StyleSheet.create({
  additionalContainer: {
    flexDirection: 'row',
  },
  additionalIconContainer: {
    justifyContent: 'center',
  },
  additionalIcon: {
    width: Scale.Horizontal(Spacing.md),
    height: Scale.Horizontal(Spacing.md),
  },
});
