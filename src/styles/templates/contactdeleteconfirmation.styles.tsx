import {StyleSheet} from 'react-native';
import {Spacing, Scale, fontSizes, fontFamilies, Colors} from '../abstract';

export default StyleSheet.create({
  outerContainer: {
    backgroundColor: Colors.white,
    marginHorizontal: Scale.Horizontal(Spacing.md),
    borderRadius: Scale.Horizontal(Spacing.xs),
  },
  alertImg: {
    width: Scale.Horizontal(100),
    height: Scale.Horizontal(100),
    alignSelf: 'center',
    marginVertical: Scale.Horizontal(Spacing.md),
  },
  mainTxt: {
    fontSize: Scale.Horizontal(fontSizes.md),
    fontFamily: fontFamilies.Regular,
    color: Colors.black,
    textAlign: 'center',
    marginBottom: Scale.Horizontal(Spacing.md),
  },
  btnContainer: {
    marginHorizontal: Scale.Horizontal(Spacing.md),
    marginBottom: Scale.Horizontal(Spacing.md),
  },
});
