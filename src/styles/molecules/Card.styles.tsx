import {StyleSheet} from 'react-native';
import {Colors, Spacing, Scale} from '../abstract';

export default StyleSheet.create({
  outerContainer: {
    backgroundColor: Colors.white,
    elevation: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    borderRadius: Scale.Horizontal(Spacing.xs),
    marginVertical: Scale.Horizontal(Spacing.xxxs),
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: Scale.Horizontal(Spacing.md),
  },
});
