import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  outerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnContainer: {
    width: '47%',
  },
});
