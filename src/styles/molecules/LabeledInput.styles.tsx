import {StyleSheet} from 'react-native';
import {Colors, fontFamilies, fontSizes, Scale} from '../abstract';

export default StyleSheet.create({
  labelTxt: {
    fontFamily: fontFamilies.Black,
    fontSize: Scale.Horizontal(fontSizes.md),
    color: Colors.black,
  },
  errorTxt: {
    color: Colors['indian-red'],
    fontFamily: fontFamilies.Regular,
    fontSize: fontSizes.xs,
  },
});
