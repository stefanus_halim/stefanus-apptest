import {StyleProp, ViewStyle, TextStyle} from 'react-native';
import {Scale, Colors, Spacing, fontSizes, fontFamilies} from '../abstract';

export type ButtonStyles = {
  container: StyleProp<ViewStyle>;
  btnText: StyleProp<TextStyle>;
  loading: StyleProp<TextStyle>;
};

export type ButtonStylesContext = {
  type: string;
};

export const getStyles = ({type}: ButtonStylesContext): ButtonStyles => ({
  container: [
    {
      borderRadius: Scale.Horizontal(8),
      justifyContent: 'center',
      width: '100%',
      elevation: 5,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
    },
    type === 'primary' && {
      backgroundColor: Colors['indian-red'],
      borderWidth: 1,
      borderColor: Colors['indian-red'],
    },
    type === 'secondary' && {
      backgroundColor: Colors.white,
      borderWidth: 1,
      borderColor: Colors['indian-red'],
    },
  ],
  btnText: [
    {
      textAlign: 'center',
      paddingVertical: Scale.Horizontal(Spacing.xs),
      fontFamily: fontFamilies.Semibold,
      fontSize: Scale.Horizontal(fontSizes.md),
    },
    type === 'primary' && {
      color: Colors.white,
    },
    type === 'secondary' && {
      color: Colors['indian-red'],
    },
  ],
  loading: [
    {
      alignSelf: 'center',
      paddingVertical: Scale.Horizontal(Spacing.md),
    },
    type === 'primary' && {
      color: Colors.white,
    },
    type === 'secondary' && {
      color: Colors['indian-red'],
    },
  ],
});
