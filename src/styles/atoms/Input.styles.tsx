import {StyleSheet} from 'react-native';
import {Scale, Spacing, fontSizes, fontFamilies, Colors} from '../abstract';

export default StyleSheet.create({
  container: {
    borderWidth: Scale.Horizontal(0.5),
    borderRadius: Scale.Horizontal(Spacing.xs),
    width: Scale.Horizontal(309),
    borderColor: Colors.black,
    marginVertical: Scale.Horizontal(Spacing.xs),
    paddingHorizontal: Scale.Horizontal(Spacing.xs),
    paddingVertical: Scale.Horizontal(Spacing.s),
    fontFamily: fontFamilies.Regular,
    fontSize: fontSizes.s,
    color: Colors.black,
  },
});
