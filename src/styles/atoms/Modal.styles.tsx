import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  modalContainer: {
    marginHorizontal: 0,
    marginVertical: 0,
  },
});
