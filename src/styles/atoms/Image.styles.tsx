import {StyleSheet} from 'react-native';
import {Spacing, Scale} from '../abstract';

export default StyleSheet.create({
  image: {
    width: Scale.Horizontal(Spacing.lg),
    height: Scale.Horizontal(Spacing.lg),
  },
});
