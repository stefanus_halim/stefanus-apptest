/**========================================
  TYPOGRAPHY (BASE DESIGN STYLING SYSTEM)
==========================================*/
export const fontFamilies = {
  Black: 'Lato-Black',
  Bold: 'Lato-Bold',
  Light: 'Lato-Light',
  Medium: 'Lato-Medium',
  Regular: 'Lato-Regular',
  Semibold: 'Lato-Semibold',
};

export const fontSizes = {
  xs: 12,
  s: 14,
  md: 16,
  lg: 18,
  xl: 20,
  xxl: 22,
};
