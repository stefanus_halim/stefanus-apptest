/**======================================
  SPACINGS (BASE DESIGN STYLING SYSTEM)
=========================================*/
export const Spacing = {
  xxxs: 8,
  xxs: 10,
  xs: 12,
  s: 14,
  md: 16,
  lg: 18,
  xl: 20,
  xxl: 22,
};
