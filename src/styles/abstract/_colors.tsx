/**====================================
  COLORS (BASE DESIGN STYLING SYSTEM)
=======================================*/
export const Colors = {
  white: '#FFFFFF',
  black: '#000000',
  'indian-red': '#CD5C5C',
  'light-grey': '#949494',
  lime: '#00FF00',
};
