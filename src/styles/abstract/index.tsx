import {Colors} from './_colors';
import {Scale} from './_scale';
import {fontFamilies} from './_typography';
import {fontSizes} from './_typography';
import {Spacing} from './_spacing';

export {Colors, Scale, fontFamilies, fontSizes, Spacing};
