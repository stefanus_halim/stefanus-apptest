/**=======================================
  SCALING (BASE DESIGN GUIDELINE SYSTEM)
==========================================*/

import {PixelRatio, Dimensions} from 'react-native';
const guidelineBaseWidth = 375;
const guidelineBaseHeight = 764;

export const Scale = {
  ScreenWidth: (value = 1) => Dimensions.get('window').width * value,
  ScreenHeight: (value = 1) => Dimensions.get('window').height * value,
  Vertical: (size: number) =>
    PixelRatio.roundToNearestPixel(
      (Dimensions.get('window').height / guidelineBaseHeight) * size,
    ),
  Horizontal: (size: number) =>
    PixelRatio.roundToNearestPixel(
      (Dimensions.get('window').width / guidelineBaseWidth) * size,
    ),
};
