const StackOptions = {
  header: () => null,
  animation: 'slide_from_right',
};

export default StackOptions;
