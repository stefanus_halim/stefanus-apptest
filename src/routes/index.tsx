import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack';
import {RouteProps} from './types';
import StackOptions from './options';

//List Screen
import ContactFormScreen from '../screens/ContactForm';
import ContactDetailScreen from '../screens/ContactDetail';
import ContactListScreen from '../screens/ContactList';

const AppStack = createNativeStackNavigator();
const AppRoute: React.FC<RouteProps> = ({initialRoute}) => {
  return (
    <NavigationContainer>
      <AppStack.Navigator initialRouteName={initialRoute}>
        <AppStack.Screen
          name="Contact List"
          component={ContactListScreen}
          options={StackOptions as NativeStackNavigationOptions}
        />
        <AppStack.Screen
          name="Contact Form"
          component={ContactFormScreen}
          options={StackOptions as NativeStackNavigationOptions}
        />
        <AppStack.Screen
          name="Contact Detail"
          component={ContactDetailScreen}
          options={StackOptions as NativeStackNavigationOptions}
        />
      </AppStack.Navigator>
    </NavigationContainer>
  );
};

export default AppRoute;
