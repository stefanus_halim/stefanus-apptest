import React, {useState, useCallback} from 'react';
import {SafeAreaView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  reqContactDetail,
  setContactDetail,
  deleteContact,
  setContactError,
  removeContactError,
  removeContactSuccess,
} from '../../stores/reducers';
import {RootState} from '../../stores/reducers';
import withFetchingData from '../../helpers/withFetch';
import {ContactDetailProps} from './types';
import ToastMessage from '../../components/atoms/Toast';
import Header from '../../components/organisms/Header';
import ContactDetail from '../../components/templates/ContactDetail';
import styles from '../../styles';

let DeleteConfirmationModal: any = null;

const ContactDetailWithFetch = withFetchingData(
  ContactDetail,
  reqContactDetail,
  setContactDetail,
);

const ContactDetailScreen: React.FC<ContactDetailProps> = ({
  navigation,
  route,
}) => {
  const id = route?.params?.id;
  const dispatch = useDispatch();
  const {detail, error, success} = useSelector(
    (state: RootState) => state.contact,
  );
  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
  const [loadingDelete, setLoadingDelete] = useState(false);

  const removeMessage = useCallback(() => {
    dispatch(removeContactSuccess());
    dispatch(removeContactError());
  }, [dispatch]);

  const handleNavigateBack = () => {
    navigation.goBack();
  };

  const handleShowDeleteConfirmation = () => {
    if (!DeleteConfirmationModal) {
      DeleteConfirmationModal =
        require('../../components/templates/ContactDeleteConfirmation').default;
    }
    setShowDeleteConfirmation(!showDeleteConfirmation);
  };

  const handleConfirmDelete = async () => {
    setLoadingDelete(true);
    try {
      const response = await dispatch(deleteContact({id}));
      console.log(response);
      setLoadingDelete(false);
      handleShowDeleteConfirmation();
      handleNavigateBack();
      // eslint-disable-next-line no-catch-shadow
    } catch (error: any) {
      setLoadingDelete(false);
      dispatch(setContactError(error.response.data.message));
      handleShowDeleteConfirmation();
    }
  };

  return (
    <>
      <SafeAreaView style={styles.pageOuter}>
        <Header
          navigation={navigation}
          showBackNav={true}
          title={'Contact Detail'}
        />
        <ContactDetailWithFetch
          navigation={navigation}
          additionalParams={{id}}
          contactDetail={detail}
          handleShowDeleteConfirmation={handleShowDeleteConfirmation}
        />
      </SafeAreaView>
      {showDeleteConfirmation && (
        <DeleteConfirmationModal
          showModal={showDeleteConfirmation}
          handleShowModal={handleShowDeleteConfirmation}
          loadingDelete={loadingDelete}
          handleConfirmDelete={handleConfirmDelete}
        />
      )}
      {(error || success) && (
        <ToastMessage
          type={success ? 'success' : 'error'}
          message={success || error}
          removeMessage={removeMessage}
        />
      )}
    </>
  );
};

export default ContactDetailScreen;
