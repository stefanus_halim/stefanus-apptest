import React, {useCallback} from 'react';
import {SafeAreaView, View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {
  reqContactList,
  setContactList,
  removeContactError,
  removeContactSuccess,
} from '../../stores/reducers';
import {RootState} from '../../stores/reducers';
import withFetchingData from '../../helpers/withFetch';
import {ContactListProps} from './types';
import List from '../../components/organisms/List';
import Card from '../../components/molecules/Card';
import ToastMessage from '../../components/atoms/Toast';
import ContactListHeader from '../../components/templates/ContactListHeader';
import ContactCardContent from '../../components/templates/ContactCardContent';
import styles from '../../styles';

const ContactListWithFetch = withFetchingData(
  List,
  reqContactList,
  setContactList,
);

const ContactListScreen: React.FC<ContactListProps> = ({navigation}) => {
  const dispatch = useDispatch();
  const {list, success} = useSelector((state: RootState) => state.contact);

  const removeMessage = useCallback(() => {
    dispatch(removeContactSuccess());
    dispatch(removeContactError());
  }, [dispatch]);

  const renderContactList = useCallback(
    (item: any, index: number) => {
      const {firstName, lastName, photo, age} = item;
      const onClickAction = () => {
        navigation.navigate('Contact Detail', {id: item.id});
      };
      return (
        <Card
          clickAction={onClickAction}
          isClickable={true}
          content={
            <View key={index}>
              <ContactCardContent
                firstName={firstName}
                lastName={lastName}
                photo={photo}
                age={age}
              />
            </View>
          }
        />
      );
    },
    [navigation],
  );

  return (
    <>
      <SafeAreaView style={styles.pageOuter}>
        <ContactListHeader navigation={navigation} />
        <ContactListWithFetch
          navigation={navigation}
          list={list}
          renderList={renderContactList}
        />
      </SafeAreaView>
      {success && (
        <ToastMessage
          type={'success'}
          message={success}
          removeMessage={removeMessage}
        />
      )}
    </>
  );
};

export default ContactListScreen;
