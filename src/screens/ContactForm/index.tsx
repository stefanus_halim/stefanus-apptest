import React, {useState, useEffect} from 'react';
import {SafeAreaView} from 'react-native';
import Header from '../../components/organisms/Header';
import ContactInfoForm from '../../components/templates/ContactInfoForm';
import formFormatter from '../../helpers/formFormatter';
import {ContactInfoFormFormat} from '../../utils/ContactInfoForm';
import {PropTypes} from './types';
import {
  setContactForm,
  updateContactDetail,
  addContact,
  removeContactForm,
  setContactSuccess,
} from '../../stores/reducers';
import {RootState} from '../../stores/reducers';
import {useSelector, useDispatch} from 'react-redux';
import styles from '../../styles';

const ContactFormScreen: React.FC<PropTypes> = ({navigation, route}) => {
  const dispatch = useDispatch();
  const form = useSelector((state: RootState) => state.contact.form);
  const [loadingSubmit, setLoadingSubmit] = useState(false);

  useEffect(() => {
    const initialForm = formFormatter.formInitFormatter(
      ContactInfoFormFormat,
      route?.params.data,
    );
    dispatch(setContactForm(initialForm));
  }, [dispatch, route?.params.data]);

  const submitDataConstructor = () => {
    let submitData = {
      id: null,
      updateData: formFormatter.formSubmitFormatter(form),
    };
    if (route && route.params && route.params.data) {
      submitData.id = route?.params.data.id;
    }
    return submitData;
  };

  const handleNavigateBack = () => {
    navigation.goBack();
  };

  const onSubmit = async () => {
    setLoadingSubmit(true);
    try {
      const submitData = submitDataConstructor();
      const isEditForm = route?.params.data ? true : false;
      const response = isEditForm
        ? await dispatch(updateContactDetail(submitData))
        : await dispatch(addContact(submitData));
      dispatch(removeContactForm());
      dispatch(setContactSuccess(response));
      setLoadingSubmit(false);
      handleNavigateBack();
    } catch (error: any) {
      const {validation, message} = error.response.data;
      const formattedForm = formFormatter.formErrorAdder(
        form,
        validation.keys[0],
        message,
      );
      dispatch(setContactForm(formattedForm));
      setLoadingSubmit(false);
    }
  };

  const onChangeFormValue = (key: string, value: string) => {
    let tempForm = [...form];
    let filteredForm = tempForm.filter(item => item.key === key);
    let indexForm = tempForm.indexOf(filteredForm[0]);
    filteredForm[0].value = value;
    filteredForm[0].error = null;
    tempForm.splice(indexForm, 1, filteredForm[0]);
    dispatch(setContactForm(tempForm));
  };

  return (
    <SafeAreaView style={styles.pageOuter}>
      <Header
        navigation={navigation}
        showBackNav={true}
        title={`${route?.params.data ? 'Edit' : 'Add'} Contact`}
      />
      <ContactInfoForm
        format={form}
        isEditContact={route?.params.data ? true : false}
        onChangeValue={onChangeFormValue}
        loadingSubmit={loadingSubmit}
        onSubmit={onSubmit}
      />
    </SafeAreaView>
  );
};

export default ContactFormScreen;
