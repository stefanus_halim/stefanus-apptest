export const ContactInfoFormFormat = [
  {
    id: 0,
    key: 'firstName',
    label: 'First Name',
    placeholder: 'Fill your first name ...',
    inputType: 'fill-text',
    keyboardType: 'default',
  },
  {
    id: 1,
    key: 'lastName',
    label: 'Last Name',
    placeholder: 'Fill your last name ...',
    inputType: 'fill-text',
    keyboardType: 'default',
  },
  {
    id: 2,
    key: 'age',
    label: 'Age',
    placeholder: 'Fill your age ...',
    inputType: 'fill-number',
    keyboardType: 'numeric',
  },
  {
    id: 3,
    key: 'photo',
    label: 'Photo URL',
    placeholder: 'Fill your photo url ...',
    inputType: 'fill-text',
    keyboardType: 'default',
  },
];
