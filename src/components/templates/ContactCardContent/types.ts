export type ContactCardContentProps = {
  age: number;
  photo: string;
  firstName: string;
  lastName: string;
};
