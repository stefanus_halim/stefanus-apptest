import React from 'react';
import {View, Text} from 'react-native';
import Image from '../../atoms/Image';
import {ContactCardContentProps} from './types';
import styles from '../../../styles/templates/contactcardcontent.styles';

const ContactCardContent: React.FC<ContactCardContentProps> = ({
  firstName,
  lastName,
  photo,
  age,
}) => {
  return (
    <View style={styles.outerContainer}>
      <View style={styles.leftContainer}>
        <Image
          image={
            !photo.includes('http')
              ? require('../../../assets/images/user-default.webp')
              : photo
          }
          imageStyle={styles.photo}
        />
      </View>
      <View style={styles.rightContainer}>
        <Text style={styles.mainTxt}>
          {firstName} {lastName}
        </Text>
        <Text style={styles.secondaryTxt}>Age: {age}</Text>
      </View>
    </View>
  );
};

export default ContactCardContent;
