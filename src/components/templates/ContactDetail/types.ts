export type ContactDetailProps = {
  firstName: string;
  lastName: string;
  age: number;
  photo: string;
};

export type DetailProps = {
  contactDetail: ContactDetailProps;
  isLoading: boolean;
  navigation: any;
  handleShowDeleteConfirmation: () => void;
};
