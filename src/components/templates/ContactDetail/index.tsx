import React from 'react';
import {View, Text} from 'react-native';
import {DetailProps} from './types';
import CircularLoader from '../../atoms/CircularLoader';
import Card from '../../molecules/Card';
import Image from '../../atoms/Image';
import RowButtons from '../../molecules/RowButtons';
import styles from '../../../styles/templates/contactdetail.styles';

const ContactDetail: React.FC<DetailProps> = ({
  contactDetail,
  isLoading,
  navigation,
  handleShowDeleteConfirmation,
}) => {
  const handleNavigateContactEdit = () => {
    navigation.navigate('Contact Form', {data: contactDetail});
  };

  return (
    <View style={styles.outerContainer}>
      {isLoading ? (
        <>
          <CircularLoader />
        </>
      ) : (
        <>
          <Card
            content={
              <View style={styles.cardContainer}>
                <Image
                  image={
                    contactDetail
                      ? !contactDetail.photo.includes('http')
                        ? require('../../../assets/images/user-default.webp')
                        : contactDetail.photo
                      : ''
                  }
                  imageStyle={styles.photo}
                />
                <View style={styles.cardContentContainer}>
                  <Text
                    style={styles.mainTxt}
                    ellipsizeMode={'tail'}
                    numberOfLines={1}>
                    {contactDetail?.firstName} {contactDetail?.lastName}
                  </Text>
                  <Text style={styles.secondaryTxt}>
                    Age: {contactDetail?.age}
                  </Text>
                </View>
              </View>
            }
            isClickable={false}
          />
          <RowButtons
            leftBtnTxt={'Delete'}
            rightBtnTxt={'Edit'}
            leftBtnAction={handleShowDeleteConfirmation}
            rightBtnAction={handleNavigateContactEdit}
            isLoadingLeft={false}
            isLoadingRight={false}
          />
        </>
      )}
    </View>
  );
};

export default ContactDetail;
