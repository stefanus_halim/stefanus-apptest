export type PropTypes = {
  format: any;
  onChangeValue: (key: string, value: string) => void;
  onSubmit: () => void;
  loadingSubmit: boolean;
  isEditContact: boolean;
};
