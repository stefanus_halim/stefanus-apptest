import React from 'react';
import {ScrollView} from 'react-native';
import Form from '../../organisms/Form';
import Button from '../../atoms/Button';
import {PropTypes} from './types';
import styles from '../../../styles/templates/contactinfoform.styles';

const ContactInfoForm: React.FC<PropTypes> = ({
  format,
  onChangeValue,
  onSubmit,
  loadingSubmit,
  isEditContact,
}) => {
  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.contentContainer}>
      <Form format={format} onChangeValue={onChangeValue} />
      <Button
        btnTxt={`${isEditContact ? 'Edit' : 'Add'} Contact`}
        type={'primary'}
        btnAction={onSubmit}
        isLoading={loadingSubmit}
      />
    </ScrollView>
  );
};

export default ContactInfoForm;
