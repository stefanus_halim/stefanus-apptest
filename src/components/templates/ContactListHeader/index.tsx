import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import Header from '../../organisms/Header';
import FastImage from 'react-native-fast-image';
import {PropTypes} from './types';
import styles from '../../../styles/templates/contactlistheader.styles';

const ContactListHeader: React.FC<PropTypes> = ({navigation}) => {
  const handleNavigateContactAdd = () => {
    navigation.navigate('Contact Form', {data: null});
  };
  return (
    <Header
      navigation={navigation}
      showBackNav={false}
      title={'Contact List'}
      additionalContent={
        <TouchableOpacity
          style={styles.additionalContainer}
          onPress={handleNavigateContactAdd}>
          <View style={styles.additionalIconContainer}>
            <FastImage
              source={require('../../../assets/images/plus.webp')}
              style={styles.additionalIcon}
            />
          </View>
        </TouchableOpacity>
      }
    />
  );
};

export default ContactListHeader;
