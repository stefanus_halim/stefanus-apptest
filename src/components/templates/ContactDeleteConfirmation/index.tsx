import React from 'react';
import {View, Text} from 'react-native';
import Modal from '../../atoms/Modal';
import FastImage from 'react-native-fast-image';
import RowButtons from '../../molecules/RowButtons';
import {PropTypes} from './types';
import styles from '../../../styles/templates/contactdeleteconfirmation.styles';

const ContactDeleteConfirmation: React.FC<PropTypes> = ({
  showModal,
  handleShowModal,
  loadingDelete,
  handleConfirmDelete,
}) => {
  return (
    <Modal
      showModal={showModal}
      handleShowModal={handleShowModal}
      content={
        <View style={styles.outerContainer}>
          <FastImage
            source={require('../../../assets/images/warning-sign.webp')}
            style={styles.alertImg}
          />
          <Text style={styles.mainTxt}>
            Are you sure want to delete this contact?
          </Text>
          <View style={styles.btnContainer}>
            <RowButtons
              leftBtnTxt={'Cancel'}
              rightBtnTxt={'Delete'}
              leftBtnAction={handleShowModal}
              rightBtnAction={handleConfirmDelete}
              isLoadingLeft={false}
              isLoadingRight={loadingDelete}
            />
          </View>
        </View>
      }
    />
  );
};

export default ContactDeleteConfirmation;
