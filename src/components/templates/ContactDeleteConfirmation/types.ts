export type PropTypes = {
  showModal: boolean;
  handleShowModal: () => void;
  loadingDelete: boolean;
  handleConfirmDelete: () => void;
};
