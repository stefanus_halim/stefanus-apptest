import React from 'react';

export type ListProps = {
  isLoading: boolean;
  isError: any;
  list: Array<any>;
  refresh: boolean;
  onRefresh: () => void;
  renderList: (param1: any, param2: number) => React.ReactElement;
};
