import React from 'react';
import {FlatList, RefreshControl} from 'react-native';
import CircularLoader from '../../atoms/CircularLoader';
import {ListProps} from './types';
import styles from '../../../styles/organisms/List.styles';

const List: React.FC<ListProps> = ({
  isLoading,
  list,
  renderList,
  refresh,
  onRefresh,
}) => {
  return (
    <FlatList
      style={styles.outerContainer}
      contentContainerStyle={styles.contentContainer}
      data={list}
      ListFooterComponent={isLoading ? <CircularLoader /> : <></>}
      showsVerticalScrollIndicator={false}
      renderItem={({item, index}) => renderList(item, index)}
      refreshControl={
        <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
      }
    />
  );
};

export default List;
