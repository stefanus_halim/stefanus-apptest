import React from 'react';
import {View} from 'react-native';
import Card from '../../molecules/Card';
import LabeledInput from '../../molecules/LabeledInput';
import {PropTypes} from './types';

const Form: React.FC<PropTypes> = ({format, onChangeValue}) => {
  return (
    <Card
      isClickable={false}
      content={
        <View>
          {format.map((item, index) => (
            <View key={index}>
              <LabeledInput
                label={item.label}
                placeholder={item.placeholder}
                value={item.value}
                onChangeText={text => onChangeValue(item.key, text)}
                keyboardType={item.keyboardType}
                error={item.error}
              />
            </View>
          ))}
        </View>
      }
    />
  );
};

export default Form;
