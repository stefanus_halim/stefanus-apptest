type FormatTypes = {
  id: number;
  key: string;
  label: string;
  placeholder: string;
  inputType: string;
  keyboardType: string;
  value: any;
  onChangeValue: () => void;
  error?: any;
};

export type PropTypes = {
  format: Array<FormatTypes>;
  onChangeValue: (key: string, value: string) => void;
};
