import React from 'react';

export type PropTypes = {
  showBackNav: boolean;
  title: string;
  navigation: any;
  additionalContent?: React.ReactElement;
};
