import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import {PropTypes} from './types';
import styles from '../../../styles/organisms/Header.styles';

const Header: React.FC<PropTypes> = ({
  title,
  showBackNav,
  navigation,
  additionalContent,
}) => {
  const handleNavigateBack = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <View style={styles.leftContainer}>
        {showBackNav && (
          <TouchableOpacity
            style={styles.backIconContainer}
            onPress={handleNavigateBack}>
            <FastImage
              source={require('../../../assets/images/left-arrow.webp')}
              style={styles.backIcon}
            />
          </TouchableOpacity>
        )}
        <Text style={styles.title}>{title}</Text>
      </View>
      <View style={styles.rightContainer}>{additionalContent}</View>
    </View>
  );
};

export default Header;
