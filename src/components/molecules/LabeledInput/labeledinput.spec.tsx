import React from 'react';
import LabeledInput from '.';
import renderer from 'react-test-renderer';

describe('Labeled Input', () => {
  it('should render proper LabeledInput without issues', () => {
    let testValue = 'Test';
    const mockFn = jest.fn();
    const testLabel = 'Test';
    const placeholder = 'Test';
    const component = renderer.create(
      <LabeledInput
        value={testValue}
        onChangeText={mockFn}
        placeholder={placeholder}
        label={testLabel}
      />,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render proper Label without issues', () => {
    let testValue = 'Test';
    const mockFn = jest.fn();
    const testLabel = 'Test';
    const placeholder = 'Test';
    const component = renderer.create(
      <LabeledInput
        value={testValue}
        onChangeText={mockFn}
        placeholder={placeholder}
        label={testLabel}
      />,
    );

    const tree: any = component.toJSON();
    expect(tree?.children[0].children[0]).toBe(testLabel);
  });

  it('should render proper Error Text Message without issues', () => {
    let testValue = 'Test';
    const mockFn = jest.fn();
    const testLabel = 'Test';
    const testError = 'Error';
    const placeholder = 'Test';
    const component = renderer.create(
      <LabeledInput
        value={testValue}
        onChangeText={mockFn}
        placeholder={placeholder}
        label={testLabel}
        error={testError}
      />,
    );

    const tree: any = component.toJSON();
    expect(tree?.children[2].children[0]).toBe(testError);
  });
});
