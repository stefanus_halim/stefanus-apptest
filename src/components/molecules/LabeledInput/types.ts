export type PropTypes = {
  label: string;
  value: any;
  onChangeText: (text: string) => void;
  keyboardType?: any;
  placeholder: string;
  error?: any;
};
