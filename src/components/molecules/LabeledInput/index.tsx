import React from 'react';
import {View, Text} from 'react-native';
import Input from '../../atoms/Input';
import {PropTypes} from './types';
import styles from '../../../styles/molecules/LabeledInput.styles';

const LabeledInput: React.FC<PropTypes> = ({
  label,
  value,
  onChangeText,
  keyboardType,
  placeholder,
  error,
}) => {
  return (
    <View>
      <Text style={styles.labelTxt}>{label}</Text>
      <Input
        value={value}
        onChangeText={onChangeText}
        keyboardType={keyboardType}
        placeholder={placeholder}
      />
      {error && <Text style={styles.errorTxt}>{error}</Text>}
    </View>
  );
};

export default LabeledInput;
