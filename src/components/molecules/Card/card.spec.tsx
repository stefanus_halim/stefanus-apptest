import React from 'react';
import {View, Text} from 'react-native';
import Card from '.';
import renderer from 'react-test-renderer';
import ShallowRenderer from 'react-test-renderer/shallow';

describe('Card', () => {
  const shallowRenderer = ShallowRenderer.createRenderer();

  it('should render proper Card without issues', () => {
    const component = renderer.create(
      <Card
        isClickable={false}
        content={
          <View>
            <Text>Test</Text>
          </View>
        }
      />,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should call onpress when card is clickable without issues', () => {
    const result = 'I was pressed';
    const mockFn = jest.fn(() => result);
    const component: any = shallowRenderer.render(
      <Card
        isClickable={true}
        clickAction={mockFn}
        content={
          <View>
            <Text>Test</Text>
          </View>
        }
      />,
    );

    component.props.children.props.onPress();

    expect(mockFn.mock.calls.length).toBe(1);
    expect(component.props.children.props.onPress()).toBe(result);
  });
});
