import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {CardProps} from './types';
import styles from '../../../styles/molecules/Card.styles';

const Card: React.FC<CardProps> = ({isClickable, content, clickAction}) => {
  return (
    <>
      {isClickable ? (
        <TouchableOpacity style={styles.outerContainer} onPress={clickAction}>
          <View style={styles.innerContainer}>{content}</View>
        </TouchableOpacity>
      ) : (
        <View style={styles.outerContainer}>
          <View style={styles.innerContainer}>{content}</View>
        </View>
      )}
    </>
  );
};

export default Card;
