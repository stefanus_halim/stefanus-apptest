import React from 'react';

export type CardProps = {
  isClickable: boolean;
  clickAction?: () => void;
  content: React.ReactElement;
};
