import React from 'react';
import {View} from 'react-native';
import {PropTypes} from './types';
import Button from '../../atoms/Button';
import styles from '../../../styles/molecules/RowButtons.styles';

const RowButtons: React.FC<PropTypes> = ({
  leftBtnTxt,
  rightBtnTxt,
  leftBtnAction,
  rightBtnAction,
  isLoadingLeft,
  isLoadingRight,
}) => {
  return (
    <View style={styles.outerContainer}>
      <View style={styles.btnContainer}>
        <Button
          btnTxt={leftBtnTxt}
          btnAction={!isLoadingLeft && !isLoadingRight && leftBtnAction}
          type={'secondary'}
          isLoading={isLoadingLeft}
          isDisable={isLoadingLeft || isLoadingRight}
        />
      </View>
      <View style={styles.btnContainer}>
        <Button
          btnTxt={rightBtnTxt}
          btnAction={!isLoadingLeft && !isLoadingRight && rightBtnAction}
          type={'primary'}
          isLoading={isLoadingRight}
          isDisable={isLoadingLeft || isLoadingRight}
        />
      </View>
    </View>
  );
};

export default RowButtons;
