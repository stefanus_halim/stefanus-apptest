export type PropTypes = {
  leftBtnTxt: string;
  rightBtnTxt: string;
  leftBtnAction: () => void;
  rightBtnAction: () => void;
  isLoadingLeft: boolean;
  isLoadingRight: boolean;
};
