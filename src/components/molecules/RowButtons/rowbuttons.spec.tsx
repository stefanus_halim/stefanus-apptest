import React from 'react';
import RowButtons from '.';
import renderer from 'react-test-renderer';
import ShallowRenderer from 'react-test-renderer/shallow';

describe('Row Buttons', () => {
  const shallowRenderer = ShallowRenderer.createRenderer();

  it('should render proper Row Buttons without issues', () => {
    const testLeftBtnTxt = 'Test 1';
    const testRightBtnTxt = 'Test 2';
    const testIsLoadingLeft = false;
    const testIsLoadingRight = false;
    const mockLeftFn = jest.fn();
    const mockRightFn = jest.fn();

    const component = renderer.create(
      <RowButtons
        leftBtnTxt={testLeftBtnTxt}
        rightBtnTxt={testRightBtnTxt}
        leftBtnAction={mockLeftFn}
        rightBtnAction={mockRightFn}
        isLoadingLeft={testIsLoadingLeft}
        isLoadingRight={testIsLoadingRight}
      />,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render proper two row buttons without issues', () => {
    const testLeftBtnTxt = 'Test 1';
    const testRightBtnTxt = 'Test 2';
    const testIsLoadingLeft = false;
    const testIsLoadingRight = false;
    const mockLeftFn = jest.fn();
    const mockRightFn = jest.fn();

    const buttonDirection = 'row';
    const buttonCount = 2;

    const component = renderer.create(
      <RowButtons
        leftBtnTxt={testLeftBtnTxt}
        rightBtnTxt={testRightBtnTxt}
        leftBtnAction={mockLeftFn}
        rightBtnAction={mockRightFn}
        isLoadingLeft={testIsLoadingLeft}
        isLoadingRight={testIsLoadingRight}
      />,
    );

    const tree: any = component.toJSON();
    expect(tree?.props.style.flexDirection).toBe(buttonDirection);
    expect(tree?.children.length).toBe(buttonCount);
  });

  it('should not called left button onPress when loading left/right is true without issues', () => {
    const testLeftBtnTxt = 'Test 1';
    const testRightBtnTxt = 'Test 2';
    const testIsLoadingLeft = false;
    const testIsLoadingRight = true;
    const result = 'I was pressed';
    const mockLeftFn = jest.fn(() => result);
    const mockRightFn = jest.fn();

    const component: any = shallowRenderer.render(
      <RowButtons
        leftBtnTxt={testLeftBtnTxt}
        rightBtnTxt={testRightBtnTxt}
        leftBtnAction={mockLeftFn}
        rightBtnAction={mockRightFn}
        isLoadingLeft={testIsLoadingLeft}
        isLoadingRight={testIsLoadingRight}
      />,
    );

    expect(
      component.props.children[0].props.children.props.btnAction,
    ).toBeFalsy();
  });

  it('should not called right button onPress when loading left/right is true without issues', () => {
    const testLeftBtnTxt = 'Test 1';
    const testRightBtnTxt = 'Test 2';
    const testIsLoadingLeft = true;
    const testIsLoadingRight = false;
    const result = 'I was pressed';
    const mockLeftFn = jest.fn(() => result);
    const mockRightFn = jest.fn();

    const component: any = shallowRenderer.render(
      <RowButtons
        leftBtnTxt={testLeftBtnTxt}
        rightBtnTxt={testRightBtnTxt}
        leftBtnAction={mockLeftFn}
        rightBtnAction={mockRightFn}
        isLoadingLeft={testIsLoadingLeft}
        isLoadingRight={testIsLoadingRight}
      />,
    );

    expect(
      component.props.children[1].props.children.props.btnAction,
    ).toBeFalsy();
  });
});
