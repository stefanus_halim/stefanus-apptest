import React from 'react';
import Modal from 'react-native-modal';
import {PropTypes} from './types';
import styles from '../../../styles/atoms/Modal.styles';

const CustomModal: React.FC<PropTypes> = ({
  showModal,
  handleShowModal,
  content,
}) => {
  return (
    <Modal
      isVisible={showModal}
      swipeDirection={'down'}
      style={styles.modalContainer}
      onBackButtonPress={handleShowModal}
      onBackdropPress={handleShowModal}
      onSwipeComplete={handleShowModal}>
      {content}
    </Modal>
  );
};

export default CustomModal;
