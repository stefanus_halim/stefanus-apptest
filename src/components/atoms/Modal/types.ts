import React from 'react';

export type PropTypes = {
  showModal: boolean;
  handleShowModal: () => void;
  content: React.ReactElement;
};
