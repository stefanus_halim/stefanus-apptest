import React from 'react';
import {View, Text} from 'react-native';
import Modal from '.';
import renderer from 'react-test-renderer';
import ShallowRenderer from 'react-test-renderer/shallow';

describe('Modal', () => {
  const shallowRenderer = ShallowRenderer.createRenderer();

  it('should render proper Modal without issues', () => {
    const mockFn = jest.fn();
    const component = renderer.create(
      <Modal
        showModal={true}
        handleShowModal={mockFn}
        content={
          <View>
            <Text>Test</Text>
          </View>
        }
      />,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should called back button press props without issues', () => {
    const result = 'I was Pressed';
    const mockFn = jest.fn(() => result);
    const component: any = shallowRenderer.render(
      <Modal
        showModal={true}
        handleShowModal={mockFn}
        content={
          <View>
            <Text>Test</Text>
          </View>
        }
      />,
    );

    component.props.onBackButtonPress();

    expect(mockFn.mock.calls.length).toBe(1);
    expect(component.props.onBackButtonPress()).toBe(result);
  });

  it('should called backdrop press props without issues', () => {
    const result = 'I was Pressed';
    const mockFn = jest.fn(() => result);
    const component: any = shallowRenderer.render(
      <Modal
        showModal={true}
        handleShowModal={mockFn}
        content={
          <View>
            <Text>Test</Text>
          </View>
        }
      />,
    );

    component.props.onBackdropPress();

    expect(mockFn.mock.calls.length).toBe(1);
    expect(component.props.onBackdropPress()).toBe(result);
  });

  it('should called finish swipe direction props without issues', () => {
    const result = 'I was Pressed';
    const mockFn = jest.fn(() => result);
    const component: any = shallowRenderer.render(
      <Modal
        showModal={true}
        handleShowModal={mockFn}
        content={
          <View>
            <Text>Test</Text>
          </View>
        }
      />,
    );

    component.props.onSwipeComplete();

    expect(mockFn.mock.calls.length).toBe(1);
    expect(component.props.onSwipeComplete()).toBe(result);
  });
});
