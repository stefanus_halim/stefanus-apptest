import React from 'react';
import Button from '.';
import renderer from 'react-test-renderer';
import ShallowRenderer from 'react-test-renderer/shallow';

describe('Button', () => {
  const shallowRenderer = ShallowRenderer.createRenderer();
  it('should render primary button without issues', () => {
    const component: any = renderer.create(
      <Button btnTxt={'Test primary'} type={'primary'} />,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render secondary button without issues', () => {
    const component = renderer.create(
      <Button btnTxt={'Test secondary'} type={'secondary'} />,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should call onPress event', () => {
    const result = 'I was Pressed';
    const mockFn = jest.fn(() => result);
    const component: any = shallowRenderer.render(
      <Button btnAction={mockFn} btnTxt={'Test onPress'} type={'primary'} />,
    );

    component.props.onPress();

    expect(mockFn.mock.calls.length).toBe(1);
    expect(component.props.onPress()).toBe(result);
  });

  it('should not call onPress event when loading is true', async () => {
    const result = 'I was pressed';
    const mockFn = jest.fn(() => result);

    const component: any = shallowRenderer.render(
      <Button
        isLoading={true}
        btnAction={mockFn}
        btnTxt={'Test onPress'}
        type={'primary'}
      />,
    );

    expect(component.props.onPress).toBeFalsy();
  });
});
