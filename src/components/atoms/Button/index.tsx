import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import CircularLoader from '../CircularLoader';
import {PropTypes} from './types';
import {getStyles} from '../../../styles/atoms/Button.styles';

const Button: React.FC<PropTypes> = ({
  btnTxt,
  btnAction,
  type,
  isLoading,
  isDisable,
}) => {
  const styles = getStyles({type});
  return (
    <TouchableOpacity
      onPress={!isLoading && btnAction}
      style={styles.container}
      disabled={isLoading || isDisable}>
      {isLoading ? (
        <CircularLoader style={styles.loading} />
      ) : (
        <Text style={styles.btnText}>{btnTxt}</Text>
      )}
    </TouchableOpacity>
  );
};

export default Button;
