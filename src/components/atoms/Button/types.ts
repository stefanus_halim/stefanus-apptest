export type PropTypes = {
  btnTxt: string;
  btnAction?: any;
  type: string;
  isLoading?: boolean;
  isDisable?: boolean;
};
