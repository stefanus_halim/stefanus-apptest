export type PropTypes = {
  value: any;
  onChangeText: (text: string) => void;
  keyboardType?: any;
  placeholder: string;
};
