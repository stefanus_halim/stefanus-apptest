import React from 'react';
import Input from '.';
import renderer from 'react-test-renderer';
import ShallowRenderer from 'react-test-renderer/shallow';

describe('Input', () => {
  const shallowRenderer = ShallowRenderer.createRenderer();
  it('should render proper Input Field without issues', () => {
    let testValue = 'Test';
    const mockFn = jest.fn();
    const placeholder = 'Test';
    const component = renderer.create(
      <Input
        value={testValue}
        onChangeText={mockFn}
        placeholder={placeholder}
      />,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should called onChangeText Input and change value without issues', () => {
    let testValue = 'Test';
    const mockFn = jest.fn(() => {
      testValue = 'Tes';
    });
    const placeholder = 'Test';
    const component: any = shallowRenderer.render(
      <Input
        value={testValue}
        onChangeText={mockFn}
        placeholder={placeholder}
      />,
    );

    const changedTestValue = 'Tes';

    component.props.onChangeText();
    expect(testValue).toBe(changedTestValue);
  });
});
