import React from 'react';
import {TextInput} from 'react-native';
import {PropTypes} from './types';
import styles from '../../../styles/atoms/Input.styles';

const Input: React.FC<PropTypes> = ({
  value,
  onChangeText,
  keyboardType,
  placeholder,
}) => {
  return (
    <TextInput
      style={styles.container}
      value={value && `${value}`}
      onChangeText={onChangeText}
      keyboardType={keyboardType || 'default'}
      placeholder={placeholder}
    />
  );
};

export default Input;
