import React from 'react';
import Toast from '.';
import renderer from 'react-test-renderer';

describe('Toast', () => {
  it('should render proper Success Toast without issues', () => {
    let type = 'success';
    let message = 'Test';
    let removeMesage = 'Test';
    const component = renderer.create(
      <Toast type={type} message={message} removeMessage={removeMesage} />,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render proper Error Toast without issues', () => {
    let type = 'error';
    let message = 'Test';
    let removeMesage = 'Test';
    const component = renderer.create(
      <Toast type={type} message={message} removeMessage={removeMesage} />,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
