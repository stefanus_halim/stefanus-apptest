import React, {useState, useEffect} from 'react';
import Toast from 'react-native-toast-message';
import {PropTypes} from './types';

const ToastMessage: React.FC<PropTypes> = ({type, message, removeMessage}) => {
  const [onHide, setOnHide] = useState(false);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleShowToast = () => {
    setOnHide(false);
    Toast.show({
      type,
      text1: message,
      position: 'bottom',
      props: {handleHideToast},
    });
  };

  const handleHideToast = () => {
    if (!onHide) {
      if (Toast) {
        if (Toast.hide) {
          Toast.hide();
        }
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  };

  useEffect(() => {
    if (message) {
      handleShowToast();
    }
  }, [message, handleShowToast]);

  return <Toast autoHide={false} onHide={removeMessage} />;
};

export default ToastMessage;
