import React from 'react';
import {ActivityIndicator} from 'react-native';
import {PropTypes} from './types';

const CircularLoader: React.FC<PropTypes> = ({style}) => {
  return <ActivityIndicator style={style} />;
};

export default CircularLoader;
