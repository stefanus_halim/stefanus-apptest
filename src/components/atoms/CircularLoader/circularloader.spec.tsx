import React from 'react';
import CircularLoader from '.';
import renderer from 'react-test-renderer';

describe('Circular Loader', () => {
  it('should render activity indicator without issues', () => {
    const component = renderer.create(<CircularLoader />);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render activity indicator with styling without issues', () => {
    const styles = {width: 40, height: 40};
    const component = renderer.create(<CircularLoader style={styles} />);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
