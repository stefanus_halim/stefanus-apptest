import {StyleProp, TextStyle} from 'react-native';

export type PropTypes = {
  style?: StyleProp<TextStyle>;
};
