import React from 'react';
import Image from '.';
import renderer from 'react-test-renderer';

describe('Image', () => {
  it('should render fast image module with proper imageURL without issues', () => {
    const testImageURL =
      'https://test.io/wp-content/uploads/2019/02/testIO-logo-rgb-2.png';
    const component = renderer.create(<Image image={testImageURL} />);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render fast image module with local assets without issues', () => {
    const testImage = require('../../../assets/images/user-default.webp');
    const component = renderer.create(<Image image={testImage} />);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render fast image module with undefined imageURL without issues', () => {
    const testImageURL = undefined;
    const component = renderer.create(<Image image={testImageURL} />);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render fast image module with null imageURL without issues', () => {
    const testImageURL = null;
    const component = renderer.create(<Image image={testImageURL} />);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render fast image module with styling without issues', () => {
    const styles = {width: 40, height: 40};
    const testImageURL =
      'https://test.io/wp-content/uploads/2019/02/testIO-logo-rgb-2.png';
    const component = renderer.create(
      <Image image={testImageURL} imageStyle={styles} />,
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
