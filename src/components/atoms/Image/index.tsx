import React from 'react';
import FastImage from 'react-native-fast-image';
import {ImageProps} from './types';
import styles from '../../../styles/atoms/Image.styles';

const Image: React.FC<ImageProps> = ({image, imageStyle}) => {
  return (
    <FastImage
      source={
        image
          ? typeof image === 'string'
            ? {uri: image, priority: FastImage.priority.high}
            : image
          : {uri: '', priority: FastImage.priority.high}
      }
      style={imageStyle || styles.image}
    />
  );
};

export default Image;
