import {StyleProp} from 'react-native';

export type ImageProps = {
  image: any;
  imageStyle?: StyleProp<any>;
};
