import Axios from 'axios';

const Request = Axios.create({
  timeout: 30000,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
  },
});

export default Request;
