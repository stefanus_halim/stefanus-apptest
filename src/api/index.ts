import {APIRequest} from './config';

export const API = {
  getAllContacts: () =>
    APIRequest({path: '/contact', method: 'GET', data: null}),
  postContact: ({updateData}: any) =>
    APIRequest({path: '/contact', method: 'POST', data: updateData}),
  getContactById: ({id}: {id: string}) =>
    APIRequest({path: `/contact/${id}`, method: 'GET', data: null}),
  deleteContactById: ({id}: {id: string}) =>
    APIRequest({path: `/contact/${id}`, method: 'DELETE', data: null}),
  updateContactById: ({id, updateData}: {id: string; updateData: any}) =>
    APIRequest({path: `/contact/${id}`, method: 'PUT', data: updateData}),
};
