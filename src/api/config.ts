import {BASE_URL} from '@env';
import Request from './request';

type Args = {
  path: string;
  method?: any;
  data?: any;
  headers?: any;
};

type requestDataProps = {
  method?: any;
  data?: any;
  headers?: any;
};

export const APIRequest = ({path, method, data}: Args) => {
  let requestData: requestDataProps = {
    method,
    data: null,
    headers: {
      'Content-Type': 'application/json',
    },
  };
  if (method !== 'GET') {
    requestData['data'] = data;
  } else {
    delete requestData.data;
  }
  return Request(`${BASE_URL}${path}`, requestData);
};
