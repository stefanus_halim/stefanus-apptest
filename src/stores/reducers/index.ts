import {combineReducers} from 'redux';
import contact from './contact/reducer';

export const rootReducer = combineReducers({contact});
export type RootState = ReturnType<typeof rootReducer>;

export * from './contact/actions';
