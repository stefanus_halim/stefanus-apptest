import actionTypes from './constant';

type ActionType = {
  type: string;
  value: any;
};

const initialState = {
  list: [],
  detail: null,
  form: [],
  success: null,
  error: null,
};

const contact = (state = initialState, action: ActionType) => {
  switch (action.type) {
    case actionTypes.SET_CONTACT_LIST:
      return {
        ...state,
        list: action.value,
      };
    case actionTypes.REMOVE_CONTACT_LIST:
      return {
        ...state,
        list: [],
      };
    case actionTypes.SET_CONTACT_DETAIL:
      return {
        ...state,
        detail: action.value,
      };
    case actionTypes.REMOVE_CONTACT_DETAIL:
      return {
        ...state,
        detail: action.value,
      };
    case actionTypes.SET_CONTACT_FORM:
      return {
        ...state,
        form: action.value,
      };
    case actionTypes.REMOVE_CONTACT_FORM:
      return {
        ...state,
        form: [],
      };
    case actionTypes.SET_CONTACT_SUCCESS:
      return {
        ...state,
        success: action.value,
      };
    case actionTypes.REMOVE_CONTACT_SUCCESS:
      return {
        ...state,
        success: null,
      };
    case actionTypes.SET_CONTACT_ERROR:
      return {
        ...state,
        error: action.value,
      };
    case actionTypes.REMOVE_CONTACT_ERROR:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};

export default contact;
