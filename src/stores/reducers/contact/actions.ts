import {API} from '../../../api';
import actionTypes from './constant';

export const setContactList = (value: any) => ({
  type: actionTypes.SET_CONTACT_LIST,
  value,
});

export const removeContactList = () => ({
  type: actionTypes.REMOVE_CONTACT_LIST,
});

export const setContactDetail = (value: any) => ({
  type: actionTypes.SET_CONTACT_DETAIL,
  value,
});

export const removeContactDetail = () => ({
  type: actionTypes.REMOVE_CONTACT_DETAIL,
});

export const setContactForm = (value: any) => ({
  type: actionTypes.SET_CONTACT_FORM,
  value,
});

export const removeContactForm = () => ({
  type: actionTypes.REMOVE_CONTACT_FORM,
});

export const setContactSuccess = (value: any) => ({
  type: actionTypes.SET_CONTACT_SUCCESS,
  value,
});

export const removeContactSuccess = () => ({
  type: actionTypes.REMOVE_CONTACT_SUCCESS,
});

export const setContactError = (value: any) => ({
  type: actionTypes.SET_CONTACT_ERROR,
  value,
});

export const removeContactError = () => ({
  type: actionTypes.REMOVE_CONTACT_ERROR,
});

export const reqContactList = () => async () => {
  try {
    const response = await API.getAllContacts();
    const contactList = response.data.data;

    return Promise.resolve(contactList);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const reqContactDetail = (payload: any) => async () => {
  try {
    const response = await API.getContactById(payload);
    const contactDetail = response.data.data;

    return Promise.resolve(contactDetail);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const addContact = (payload: any) => async () => {
  try {
    const response = await API.postContact(payload);
    const {message} = response.data;
    return Promise.resolve(message);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deleteContact = (payload: any) => async () => {
  try {
    const response = await API.deleteContactById(payload);
    const {message} = response.data;
    return Promise.resolve(message);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const updateContactDetail = (payload: any) => async () => {
  try {
    const response = await API.updateContactById(payload);
    const {message} = response.data;
    return Promise.resolve(message);
  } catch (error) {
    return Promise.reject(error);
  }
};
