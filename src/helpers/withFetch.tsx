import React, {useEffect, useState, useCallback} from 'react';
import {useDispatch} from 'react-redux';

export default function withFetchingData(
  WrappedComponent: React.FC<any>,
  reqAction: any,
  setAction: any,
) {
  const WithFetch = (props: any) => {
    const {additionalParams} = props;
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState<any>(null);
    const [refreshing, setRefreshing] = useState(false);

    const onRefresh = () => {
      setRefreshing(true);
    };

    const fetchData = useCallback(
      async (reqAction, setAction) => {
        setIsLoading(true);
        setIsError(false);
        try {
          const responseData = additionalParams
            ? await dispatch(reqAction(additionalParams))
            : await dispatch(reqAction());
          dispatch(setAction(responseData));
          setIsLoading(false);
          setRefreshing(false);
        } catch (err) {
          setIsLoading(false);
          setRefreshing(false);
          setIsError(err);
        }
      },
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [dispatch],
    );

    useEffect(() => {
      const unsubscribe = props.navigation.addListener('focus', () => {
        if (reqAction && setAction) fetchData(reqAction, setAction);
      });
      return unsubscribe;
    });

    useEffect(() => {
      if (reqAction && setAction) fetchData(reqAction, setAction);
    }, [fetchData, refreshing]);

    return (
      <WrappedComponent
        {...props}
        isLoading={isLoading}
        isError={isError}
        refresh={refreshing}
        onRefresh={onRefresh}
      />
    );
  };

  return WithFetch;
}
