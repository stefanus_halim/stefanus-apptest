export const formFormatter = {
  formInitFormatter: (rawFormat: Array<any>, preAddedValue: Array<any>) => {
    let newFormat = [...rawFormat];
    for (let i = 0; i < newFormat.length; i++) {
      if (
        newFormat[i].inputType === 'fill-text' ||
        newFormat[i].inputType === 'fill-number'
      ) {
        newFormat[i].value = preAddedValue
          ? preAddedValue[newFormat[i].key]
          : null;
        newFormat[i].error = null;
      }
    }
    return newFormat;
  },
  formSubmitFormatter: (rawFormat: any) => {
    let baseFormat = {
      firstName: '',
      lastName: '',
      age: 0,
      photo: '',
    };
    if (Array.isArray(rawFormat)) {
      return {
        firstName: rawFormat[0].value,
        lastName: rawFormat[1].value,
        age: +rawFormat[2].value,
        photo: rawFormat[3].value,
      };
    } else {
      return baseFormat;
    }
  },
  formErrorAdder: (
    rawFormat: Array<any>,
    errorKey: string,
    errorMessage: string,
  ) => {
    let newFormat = [...rawFormat];
    let filteredInput = newFormat.filter(item => item.key === errorKey);
    let indexOfFilteredInput = newFormat.indexOf(filteredInput[0]);
    const convertedErrorMessage = errorMessage.slice(
      errorMessage.indexOf('[') + 1,
      errorMessage.indexOf(']'),
    );
    filteredInput[0].error = convertedErrorMessage;
    newFormat.splice(indexOfFilteredInput, 1, filteredInput[0]);
    return newFormat;
  },
};

export default formFormatter;
