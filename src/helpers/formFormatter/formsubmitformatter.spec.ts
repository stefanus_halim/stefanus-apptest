import {formFormatter} from './index';

describe('Form Submit Formatter Helpers', () => {
  it('should return 4 key-value object if data type is not array', () => {
    let testRawData = null;

    let testValue = formFormatter.formSubmitFormatter(testRawData);

    expect(testValue).toHaveProperty('firstName');
    expect(testValue).toHaveProperty('lastName');
    expect(testValue).toHaveProperty('age');
    expect(testValue).toHaveProperty('photo');
  });
});
